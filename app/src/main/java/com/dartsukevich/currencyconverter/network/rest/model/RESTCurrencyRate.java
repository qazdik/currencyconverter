package com.dartsukevich.currencyconverter.network.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by User on 1/30/2018.
 */

public class RESTCurrencyRate {

    @SerializedName("Cur_ID")
    private Integer currencyId;

    @SerializedName("Cur_Abbreviation")
    private String abbreviation;

    @SerializedName("Cur_Scale")
    private Float scale;

    @SerializedName("Date")
    private String date;

    @SerializedName("Cur_OfficialRate")
    private Float rate;

    public Integer getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(Integer currencyId) {
        this.currencyId = currencyId;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public Float getScale() {
        return scale;
    }

    public void setScale(Float scale) {
        this.scale = scale;
    }

    public Float getRate() {
        return rate;
    }

    public void setRate(Float rate) {
        this.rate = rate;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
