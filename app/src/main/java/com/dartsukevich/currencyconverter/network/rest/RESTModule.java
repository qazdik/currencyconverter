package com.dartsukevich.currencyconverter.network.rest;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by User on 1/30/2018.
 */

@Module
public class RESTModule {
    private static final String NBRB_BASE_URL = "http://www.nbrb.by";

    @Provides
    @Singleton
    @Named("NBRB_ENDPOINT")
    Retrofit provideCurrencyEndpoint() {
        return new Retrofit.Builder().baseUrl(NBRB_BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create()).build();
    }

    @Provides
    CurrencyApi provideCurrencyApi(@Named("NBRB_ENDPOINT") Retrofit retrofit) {
        return retrofit.create(CurrencyApi.class);
    }
}
