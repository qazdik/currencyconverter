package com.dartsukevich.currencyconverter.network.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by User on 1/30/2018.
 */

public class RESTCurrency {

    @SerializedName("Cur_ID")
    private Integer currencyId;

    @SerializedName("Cur_Abbreviation")
    private String abbreviation;

    @SerializedName("Cur_Code")
    private String code;

    @SerializedName("Cur_Name")
    private String name;

    @SerializedName("Cur_Periodicity")
    private Integer periodicity;

    @SerializedName("Cur_DateStart")
    private String dateStart;

    @SerializedName("Cur_DateEnd")
    private String dateEnd;

    public Integer getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(Integer currencyId) {
        this.currencyId = currencyId;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDateStart() {
        return dateStart;
    }

    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    public String getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    public Integer getPeriodicity() {
        return periodicity;
    }

    public void setPeriodicity(Integer periodicity) {
        this.periodicity = periodicity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
