package com.dartsukevich.currencyconverter.network.rest;

import com.dartsukevich.currencyconverter.network.rest.model.RESTCurrency;
import com.dartsukevich.currencyconverter.network.rest.model.RESTCurrencyRate;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by User on 1/30/2018.
 */

public interface CurrencyApi {

    @GET("API/ExRates/Currencies")
    Observable<List<RESTCurrency>> getCurrencyList();

    @GET("API/ExRates/Rates/{id}")
    Observable<RESTCurrencyRate> getExchangeRate(@Path("id") int currencyId);
}
