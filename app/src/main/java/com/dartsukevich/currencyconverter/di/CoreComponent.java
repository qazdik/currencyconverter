package com.dartsukevich.currencyconverter.di;

import android.app.Application;
import android.arch.lifecycle.ViewModel;

import com.dartsukevich.currencyconverter.CurrencyConverterApp;
import com.dartsukevich.currencyconverter.network.rest.RESTModule;
import com.dartsukevich.currencyconverter.repository.persistence.PersistenceModule;
import com.dartsukevich.currencyconverter.ui.ActivitiesModule;
import com.dartsukevich.currencyconverter.ui.FragmentsModule;
import com.dartsukevich.currencyconverter.ui.viewmodel.ViewModelModule;

import java.util.Map;

import javax.inject.Provider;
import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;

/**
 * Created by User on 1/30/2018.
 */

@Singleton
@Component(modules = {PersistenceModule.class, RESTModule.class, ViewModelModule.class,
        ActivitiesModule.class, FragmentsModule.class, AndroidInjectionModule.class})
public interface CoreComponent {

    void inject(CurrencyConverterApp app);

    Map<Class<? extends ViewModel>, Provider<ViewModel>> getViewModels();

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);

        CoreComponent build();
    }
}
