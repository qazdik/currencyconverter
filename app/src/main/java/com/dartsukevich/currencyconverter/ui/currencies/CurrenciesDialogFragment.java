package com.dartsukevich.currencyconverter.ui.currencies;

import android.app.Dialog;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.dartsukevich.currencyconverter.R;
import com.dartsukevich.currencyconverter.repository.persistence.model.Currency;
import com.dartsukevich.currencyconverter.ui.converter.ConverterViewModel;
import com.dartsukevich.currencyconverter.ui.viewmodel.LiveDataState;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import dagger.android.support.AndroidSupportInjection;

/**
 * Created by User on 1/31/2018.
 */

public class CurrenciesDialogFragment extends AppCompatDialogFragment {

    private CurrenciesAdapter mCurrenciesAdapter;
    private View mDialogCustomView;
    private Unbinder mUnbinder;

    @BindView(R.id.diag_currencies_rc_currencies)
    RecyclerView mCurrenciesRecyclerView;

    @BindView(R.id.diag_currencies_t_no_inet)
    TextView mNoInetTextView;

    @BindView(R.id.diag_currencies_t_retry)
    TextView mRetryTextView;

    @BindView(R.id.diag_currencies_pb_loading)
    ProgressBar mLoadingProgressBar;

    @Inject
    ViewModelProvider.Factory viewModelFactory;


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater layoutInflater = getActivity().getLayoutInflater();

        mDialogCustomView = layoutInflater.inflate(R.layout.dialog_currencies, null);
        mUnbinder = ButterKnife.bind(this, mDialogCustomView);

        return new AlertDialog.Builder(getActivity())
                .setTitle(R.string.diag_currencies_title)
                .setView(mDialogCustomView)
                .setPositiveButton(R.string.diag_currencies_btn_choose, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        final ConverterViewModel converterViewModel = ViewModelProviders.of(getActivity(), viewModelFactory).get(ConverterViewModel.class);
                        converterViewModel.selectCurrency(mCurrenciesAdapter.getSelectedCurrency());
                    }
                }).setNegativeButton(R.string.diag_currencies_btn_choose_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).create();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mUnbinder.unbind();
    }

    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    @Override
    public void onStart() {
        super.onStart();

        final ConverterViewModel converterViewModel = ViewModelProviders.of(getActivity(), viewModelFactory).get(ConverterViewModel.class);
        final AlertDialog alertDialog = (AlertDialog) getDialog();

        mRetryTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                converterViewModel.loadCurrencies();
            }
        });

        converterViewModel.getCurrencies().observe(this, new android.arch.lifecycle.Observer<LiveDataState<List<Currency>>>() {
            @Override
            public void onChanged(@Nullable LiveDataState<List<Currency>> data) {
                if (data.getState() == LiveDataState.State.ERROR) {
                    mCurrenciesRecyclerView.setVisibility(View.INVISIBLE);
                    mLoadingProgressBar.setVisibility(View.GONE);

                    mRetryTextView.setVisibility(View.VISIBLE);
                    mNoInetTextView.setVisibility(View.VISIBLE);

                    alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);

                    if (data.getT() instanceof IOException) {
                        mNoInetTextView.setText(getContext().getText(R.string.diag_currencies_error_no_internet));
                    } else {
                        mNoInetTextView.setText(getContext().getText(R.string.diag_currencies_error_unexpected));
                    }

                } else if (data.getState() == LiveDataState.State.LOADING) {
                    mCurrenciesRecyclerView.setVisibility(View.INVISIBLE);
                    mLoadingProgressBar.setVisibility(View.VISIBLE);

                    mRetryTextView.setVisibility(View.GONE);
                    mNoInetTextView.setVisibility(View.GONE);

                    alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
                } else {
                    mCurrenciesRecyclerView.setVisibility(View.VISIBLE);
                    mLoadingProgressBar.setVisibility(View.GONE);

                    mRetryTextView.setVisibility(View.GONE);
                    mNoInetTextView.setVisibility(View.GONE);
                    mCurrenciesAdapter.updateData(data.getData());

                    alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                }
            }
        });

        converterViewModel.loadCurrencies();

        mCurrenciesAdapter = new CurrenciesAdapter(getActivity().getLayoutInflater());
        mCurrenciesAdapter.setHasStableIds(true);
        mCurrenciesRecyclerView.setAdapter(mCurrenciesAdapter);
        mCurrenciesRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mCurrenciesRecyclerView.addOnChildAttachStateChangeListener(mCurrenciesAdapter);
    }

    @Override
    public void onStop() {
        super.onStop();
        mRetryTextView.setOnClickListener(null);
    }

    private static class CurrenciesAdapter extends RecyclerView.Adapter<CurrencyViewHolder> implements Observer, RecyclerView.OnChildAttachStateChangeListener {
        private List<Currency> mCurrencyList = new ArrayList<>();
        private LayoutInflater mLayoutInflater;

        private int mCheckedPos = 0;

        CurrenciesAdapter(LayoutInflater mLayoutInflater) {
            this.mLayoutInflater = mLayoutInflater;
        }

        void updateData(List<Currency> currencyList) {
            mCurrencyList.clear();
            mCurrencyList.addAll(currencyList);

            notifyDataSetChanged();
        }

        Currency getSelectedCurrency() {
            return mCurrencyList.get(mCheckedPos);
        }

        @Override
        public CurrencyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = mLayoutInflater.inflate(R.layout.item_currency, parent, false);
            return new CurrencyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(CurrencyViewHolder holder, int position) {
            Currency currency = mCurrencyList.get(position);
            CheckableCurrencyView view = holder.getView();
            view.updateData(currency, position);
            view.setChecked(mCheckedPos == position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemCount() {
            return mCurrencyList.size();
        }

        @Override
        public void onChildViewAttachedToWindow(View view) {
            CheckableCurrencyView currencyView = (CheckableCurrencyView) view;
            currencyView.subscribe(this);
        }

        @Override
        public void onChildViewDetachedFromWindow(View view) {
            CheckableCurrencyView currencyView = (CheckableCurrencyView) view;
            currencyView.unsubscribe(this);
        }

        @Override
        public void update(Observable o, Object viewState) {
            CheckableCurrencyView.ViewState checkableViewState = (CheckableCurrencyView.ViewState) viewState;
            if (checkableViewState.mChecked) {
                notifyItemChanged(mCheckedPos);
                mCheckedPos = checkableViewState.mPosition;
            }
            notifyItemChanged(mCheckedPos);
        }
    }

    private static class CurrencyViewHolder extends RecyclerView.ViewHolder {
        CurrencyViewHolder(View itemView) {
            super(itemView);
        }

        CheckableCurrencyView getView() {
            return (CheckableCurrencyView) itemView;
        }
    }
}
