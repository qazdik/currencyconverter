package com.dartsukevich.currencyconverter.ui.viewmodel;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.dartsukevich.currencyconverter.ui.ViewModelKey;
import com.dartsukevich.currencyconverter.ui.converter.ConverterViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

/**
 * Created by User on 1/31/2018.
 */

@Module
public abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(ConverterViewModel.class)
    abstract ViewModel bindConverterViewModel(ConverterViewModel converterViewModel);

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(ConverterViewModelFactory factory);
}

