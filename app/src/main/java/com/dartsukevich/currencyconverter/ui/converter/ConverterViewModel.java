package com.dartsukevich.currencyconverter.ui.converter;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.dartsukevich.currencyconverter.repository.CurrencyRepository;
import com.dartsukevich.currencyconverter.repository.persistence.model.Currency;
import com.dartsukevich.currencyconverter.repository.persistence.model.CurrencyRate;
import com.dartsukevich.currencyconverter.ui.viewmodel.LiveDataState;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by User on 1/31/2018.
 */

public class ConverterViewModel extends ViewModel {

    private MutableLiveData<LiveDataState<Currency>> mSelectedLiveData = new MutableLiveData<>();
    private MutableLiveData<LiveDataState<CurrencyRate>> mCurrencyRateLiveData = new MutableLiveData<>();
    private MutableLiveData<LiveDataState<List<Currency>>> mCurrenciesLiveData = new MutableLiveData<>();

    private CurrencyRepository mCurrencyRepository;
    private Disposable mCurrenciesDisposable;
    private Disposable mCurrencyRateDisposable;

    @Inject
    public ConverterViewModel(CurrencyRepository currencyRepository) {
        this.mCurrencyRepository = currencyRepository;
    }

    public LiveData<LiveDataState<Currency>> getSelectedCurrency() {
        return mSelectedLiveData;
    }

    public LiveData<LiveDataState<CurrencyRate>> getSelectedCurrencyRate() {
        return mCurrencyRateLiveData;
    }

    public LiveData<LiveDataState<List<Currency>>> getCurrencies() {
        return mCurrenciesLiveData;
    }

    public void loadCurrencies() {
        final Date currentDate = new Date();
        final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

        mCurrenciesLiveData.setValue(new LiveDataState<List<Currency>>(null, LiveDataState.State.LOADING));
        if (mCurrenciesDisposable != null && !mCurrenciesDisposable.isDisposed()) {
            mCurrenciesDisposable.dispose();
        }

        mCurrenciesDisposable = mCurrencyRepository.loadCurrencies()
                .map(new Function<List<Currency>, List<Currency>>() {
                    @Override
                    public List<Currency> apply(List<Currency> currencyList) throws Exception {
                        List<Currency> filteredCurrencies = new ArrayList<>();
                        for (Currency currency : currencyList) {
                            if (dateFormat.parse(currency.getDateEnd()).after(currentDate)) {
                                filteredCurrencies.add(currency);
                            }
                        }
                        return filteredCurrencies;
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<Currency>>() {
                    @Override
                    public void accept(List<Currency> currencyList) throws Exception {
                        mCurrenciesLiveData.setValue(new LiveDataState<List<Currency>>(currencyList));
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        mCurrenciesLiveData.setValue(new LiveDataState<List<Currency>>(throwable));
                    }
                });
    }

    public void loadCurrencyRate(int currencyId) {
        mCurrencyRateLiveData.setValue(new LiveDataState<CurrencyRate>(null, LiveDataState.State.LOADING));
        if (mCurrencyRateDisposable != null && !mCurrencyRateDisposable.isDisposed()) {
            mCurrencyRateDisposable.dispose();
        }

        mCurrencyRateDisposable = mCurrencyRepository.loadExchangeRate(currencyId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<CurrencyRate>() {
                    @Override
                    public void accept(CurrencyRate currencyRate) throws Exception {
                        mCurrencyRateLiveData.setValue(new LiveDataState<CurrencyRate>(currencyRate));
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        mCurrencyRateLiveData.setValue(new LiveDataState<CurrencyRate>(throwable));
                    }
                });
    }

    public void selectCurrency(Currency currency) {
        // should notify when dialog is disappeared
        mSelectedLiveData.postValue(new LiveDataState<Currency>(currency));
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        if (mCurrencyRateDisposable != null && !mCurrencyRateDisposable.isDisposed()) {
            mCurrencyRateDisposable.dispose();
        }
        if (mCurrenciesDisposable != null && !mCurrenciesDisposable.isDisposed()) {
            mCurrenciesDisposable.dispose();
        }
    }
}
