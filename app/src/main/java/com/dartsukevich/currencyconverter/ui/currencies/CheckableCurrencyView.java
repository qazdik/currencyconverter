package com.dartsukevich.currencyconverter.ui.currencies;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.widget.Checkable;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;

import com.dartsukevich.currencyconverter.R;
import com.dartsukevich.currencyconverter.repository.persistence.model.Currency;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by User on 1/31/2018.
 */

public class CheckableCurrencyView extends ConstraintLayout implements CompoundButton.OnCheckedChangeListener, Checkable {
    private ViewStateObservable checkedStateObservable = new ViewStateObservable();
    private int mPosition;

    TextView tCurrency;
    TextView tDescription;
    RadioButton rbChoose;

    public CheckableCurrencyView(Context context) {
        super(context);
    }

    public CheckableCurrencyView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CheckableCurrencyView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        prepareView();
    }

    void updateData(Currency currency, int position) {
        mPosition = position;

        tCurrency.setText(currency.getAbbreviation());
        tDescription.setText(currency.getName());
    }

    void subscribe(Observer observer) {
        checkedStateObservable.addObserver(observer);
    }

    void unsubscribe(Observer observer) {
        checkedStateObservable.deleteObserver(observer);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            checkedStateObservable.updateState(new ViewState(mPosition, isChecked));
        }
    }

    private void prepareView() {
        tCurrency = findViewById(R.id.item_cur_t_currency);
        tDescription = findViewById(R.id.item_cur_t_description);
        rbChoose = findViewById(R.id.item_cur_rb_choose);

        rbChoose.setOnCheckedChangeListener(this);
    }

    @Override
    public void setChecked(boolean checked) {
        rbChoose.setChecked(checked);
    }

    @Override
    public boolean isChecked() {
        return rbChoose.isChecked();
    }

    @Override
    public void toggle() {
        setChecked(!isChecked());
    }

    static class ViewState {
        int mPosition;
        boolean mChecked;

        ViewState(int mPosition, boolean checked) {
            this.mPosition = mPosition;
            this.mChecked = checked;
        }
    }

    static class ViewStateObservable extends Observable {
        void updateState(ViewState viewState) {
            setChanged();
            notifyObservers(viewState);
        }
    }
}
