package com.dartsukevich.currencyconverter.ui.keyboard;

/**
 * Created by User on 1/31/2018.
 */

public class KeyAction implements Cloneable {

    public enum Action {
        APPEND,
        CLEAR
    }

    private Action mAction;
    private String mData;

    public KeyAction(Action mAction, String mData) {
        this.mAction = mAction;
        this.mData = mData;
    }

    public Action getAction() {
        return mAction;
    }

    public void setAction(Action action) {
        this.mAction = action;
    }

    public String getData() {
        return mData;
    }

    public void setData(String data) {
        this.mData = data;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
