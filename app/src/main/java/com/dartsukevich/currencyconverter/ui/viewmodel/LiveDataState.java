package com.dartsukevich.currencyconverter.ui.viewmodel;

/**
 * Created by User on 1/31/2018.
 */

public class LiveDataState<T> {

    public enum State {
        SUCCESSFUL,
        LOADING,
        ERROR
    }

    private T data;
    private State state;
    private Throwable t;


    public LiveDataState(T data) {
        this.data = data;
        this.state = State.SUCCESSFUL;
    }

    public LiveDataState(Throwable t) {
        this.state = State.ERROR;
        this.t = t;
    }

    public LiveDataState(T data, State state) {
        this.data = data;
        this.state = state;
    }

    public LiveDataState(T data, State state, Throwable t) {
        this.data = data;
        this.state = state;
        this.t = t;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public State getState() {
        return state;
    }

    public boolean hasError() {
        return state == State.ERROR;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Throwable getT() {
        return t;
    }

    public void setT(Throwable t) {
        this.t = t;
    }
}
