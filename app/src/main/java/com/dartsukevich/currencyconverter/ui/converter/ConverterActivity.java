package com.dartsukevich.currencyconverter.ui.converter;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.dartsukevich.currencyconverter.R;
import com.dartsukevich.currencyconverter.repository.persistence.model.Currency;
import com.dartsukevich.currencyconverter.repository.persistence.model.CurrencyRate;
import com.dartsukevich.currencyconverter.ui.currencies.CurrenciesDialogFragment;
import com.dartsukevich.currencyconverter.ui.keyboard.DigitalKeyboardView;
import com.dartsukevich.currencyconverter.ui.keyboard.KeyAction;
import com.dartsukevich.currencyconverter.ui.viewmodel.LiveDataState;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import butterknife.Unbinder;
import dagger.android.AndroidInjection;
import io.reactivex.observers.DisposableObserver;

public class ConverterActivity extends AppCompatActivity {
    private static final String DIALOG_TAG = "CurrenciesDialogFragment";

    private CurrencyExchangeHandler mExchangeHandler;
    private Unbinder mUnbinder;
    private CurrencyRate mCurrencyRate;

    @Inject
    ViewModelProvider.Factory mViewModelFactory;

    @BindView(R.id.act_conv_et_byn)
    EditText mBynEditText;

    @BindView(R.id.act_conv_et_selected_currency)
    EditText mSelectedEditText;

    @BindView(R.id.act_conv_t_selected_currency)
    TextView mSelectedTextView;

    @BindView(R.id.act_conv_keyboard)
    DigitalKeyboardView mKeyboardView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_converter);
        mUnbinder = ButterKnife.bind(this);

        mBynEditText.setRawInputType(InputType.TYPE_CLASS_TEXT);
        mBynEditText.setTextIsSelectable(true);

        mSelectedEditText.setRawInputType(InputType.TYPE_CLASS_TEXT);
        mSelectedEditText.setTextIsSelectable(true);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mUnbinder.unbind();
    }

    @Override
    protected void onStart() {
        super.onStart();

        mBynEditText.requestFocus();

        final ConverterViewModel converterViewModel = ViewModelProviders.of(this, mViewModelFactory).get(ConverterViewModel.class);
        converterViewModel.getSelectedCurrency().observe(this, new Observer<LiveDataState<Currency>>() {
            @Override
            public void onChanged(@Nullable LiveDataState<Currency> data) {
                mSelectedTextView.setText(data.getData().getAbbreviation());
                converterViewModel.loadCurrencyRate(data.getData().getCurrencyId());
            }
        });

        converterViewModel.getSelectedCurrencyRate().observe(this, new Observer<LiveDataState<CurrencyRate>>() {
            @Override
            public void onChanged(@Nullable LiveDataState<CurrencyRate> data) {
                mCurrencyRate = data.getData();
                mExchangeHandler.setExchangeRate(mCurrencyRate);
                mExchangeHandler.convert();
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mExchangeHandler != null && !mExchangeHandler.isDisposed()) {
            mExchangeHandler.dispose();
        }
    }

    @OnClick(R.id.act_conv_t_selected_currency)
    void onClickSelectedCurrency() {
        CurrenciesDialogFragment currenciesDialogFragment = new CurrenciesDialogFragment();
        currenciesDialogFragment.show(getSupportFragmentManager(), DIALOG_TAG);
    }

    @OnFocusChange(value = {R.id.act_conv_et_selected_currency, R.id.act_conv_et_byn})
    void onFocusChange(View v, boolean focus) {

        if (mSelectedEditText.isFocused()) {
            mSelectedTextView.setTypeface(Typeface.DEFAULT_BOLD);
            changeInputTarget(mSelectedEditText, mBynEditText, mCurrencyRate, false);
        } else {
            mSelectedEditText.setTypeface(Typeface.DEFAULT);
        }

        if (mBynEditText.isFocused()) {
            mBynEditText.setTypeface(Typeface.DEFAULT_BOLD);
            changeInputTarget(mBynEditText, mSelectedEditText, mCurrencyRate, true);
        } else {
            mBynEditText.setTypeface(Typeface.DEFAULT);
        }
    }

    private void changeInputTarget(EditText targetEditText, EditText convertEditText, CurrencyRate rate, boolean isDirect) {
        if (mExchangeHandler != null && !mExchangeHandler.isDisposed()) {
            mExchangeHandler.dispose();
        }
        mExchangeHandler = mKeyboardView.listenKeyboardActions().subscribeWith(new CurrencyExchangeHandler(targetEditText, convertEditText, rate, isDirect));
    }

    private static class CurrencyExchangeHandler extends DisposableObserver<KeyAction> {
        EditText mTargetEditText;
        EditText mConvertEditText;
        CurrencyRate mRate;
        boolean mIsDirect;

        CurrencyExchangeHandler(EditText targetTextView, EditText convertTextView, CurrencyRate rate, boolean direct) {
            this.mTargetEditText = targetTextView;
            this.mConvertEditText = convertTextView;
            this.mRate = rate;
            this.mIsDirect = direct;
        }

        void convert() {
            float rate = 0;

            if (mRate != null) {
                rate = mRate.getRate() / mRate.getScale();
                rate = mIsDirect ? 1.0f / rate : rate;
            }

            try {
                double sourceValue = Float.parseFloat(mTargetEditText.getText().toString());
                double convertedValue = sourceValue * rate;
                mConvertEditText.setText(String.valueOf(convertedValue));
            } catch (NumberFormatException e) {
                e.printStackTrace();

                mTargetEditText.setText(String.valueOf(0));
                mConvertEditText.setText(String.valueOf(0));
            }
        }

        void setExchangeRate(CurrencyRate exchangeRate) {
            mRate = exchangeRate;
        }

        @Override
        public void onNext(KeyAction keyAction) {
            Editable text = mTargetEditText.getText();
            if (keyAction.getAction() == KeyAction.Action.APPEND) {
                if (text.toString().equals("0")) {
                    text.replace(0, 1, keyAction.getData(), 0, 1);
                } else {
                    text.append(keyAction.getData());
                }
            } else {
                if (text.length() > 0) {
                    text.delete(text.length() - 1, text.length());
                }
            }
            convert();
        }

        @Override
        public void onError(Throwable e) {

        }

        @Override
        public void onComplete() {

        }
    }
}
