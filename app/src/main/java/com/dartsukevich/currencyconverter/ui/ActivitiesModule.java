package com.dartsukevich.currencyconverter.ui;

import com.dartsukevich.currencyconverter.ui.converter.ConverterActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by User on 1/31/2018.
 */

@Module
public abstract class ActivitiesModule {

    @ActivityScope
    @ContributesAndroidInjector()
    abstract ConverterActivity contributeConverterActivity();

}
