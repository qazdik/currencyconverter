package com.dartsukevich.currencyconverter.ui.keyboard;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableRow;

import com.dartsukevich.currencyconverter.R;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

/**
 * Created by User on 1/31/2018.
 */

public class DigitalKeyboardView extends TableLayout {

    private Observable<KeyAction> mObservable;

    public DigitalKeyboardView(Context context) {
        super(context);
        init();
    }

    public DigitalKeyboardView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.view_keyboard, this);
        setShrinkAllColumns(true);
        setStretchAllColumns(true);

        List<Observable<KeyAction>> observables = new ArrayList<>();
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View child = getChildAt(i);

            if (child instanceof TableRow) {
                TableRow tableRow = (TableRow) child;

                for (int j = 0; j < tableRow.getChildCount(); j++) {
                    View tableChild = tableRow.getChildAt(j);

                    if (tableChild instanceof KeyboardButton) {
                        KeyboardButton keyboardButton = (KeyboardButton) tableChild;
                        observables.add(keyboardButton.listen());
                    } else {
                        throw new IllegalArgumentException("Child view must be KeyboardButton");
                    }
                }
            } else {
                throw new IllegalArgumentException("Child view must be TableRow");
            }
        }

        mObservable = Observable.merge(observables);
    }

    public Observable<KeyAction> listenKeyboardActions() {
        return mObservable;
    }
}
