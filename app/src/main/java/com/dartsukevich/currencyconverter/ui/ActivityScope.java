package com.dartsukevich.currencyconverter.ui;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;

import javax.inject.Scope;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by User on 1/31/2018.
 */

@Scope
@Documented
@Retention(RUNTIME)
public @interface ActivityScope {
}
