package com.dartsukevich.currencyconverter.ui.keyboard;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Looper;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;
import android.view.View;

import com.dartsukevich.currencyconverter.R;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.MainThreadDisposable;

/**
 * Created by User on 1/31/2018.
 */

public class KeyboardButton extends AppCompatButton {
    private KeyAction mKeyAction;

    public KeyboardButton(Context context) {
        super(context);
    }

    public KeyboardButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public KeyboardButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private void init(Context context, AttributeSet attrs) {
        TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.KeyboardButton, 0, 0);
        Integer action = typedArray.getInteger(R.styleable.KeyboardButton_action, 0);
        String data = typedArray.getString(R.styleable.KeyboardButton_action_data);

        mKeyAction = new KeyAction(KeyAction.Action.values()[action], data);
    }

    public Observable<KeyAction> listen() {
        try {
            return new ViewClickObservable(this, (KeyAction) mKeyAction.clone());
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return null;
    }

    final static class ViewClickObservable extends Observable<KeyAction> {
        private final View view;
        private final KeyAction prototype;

        ViewClickObservable(View view, KeyAction prototype) {
            this.view = view;
            this.prototype = prototype;
        }

        @Override
        protected void subscribeActual(Observer<? super KeyAction> observer) {
            if (!checkMainThread(observer)) {
                return;
            }
            Listener listener = new Listener(view, prototype, observer);
            observer.onSubscribe(listener);
            view.setOnClickListener(listener);
        }

        boolean checkMainThread(Observer<?> observer) {
            if (Looper.myLooper() != Looper.getMainLooper()) {
                observer.onError(new IllegalStateException(
                        "Expected to be called on the main thread but was " + Thread.currentThread().getName()));
                return false;
            }
            return true;
        }

        static class Listener extends MainThreadDisposable implements OnClickListener {
            private final View view;
            private final KeyAction prototype;
            private final Observer<? super KeyAction> observer;

            Listener(View view, KeyAction prototype, Observer<? super KeyAction> observer) {
                this.view = view;
                this.prototype = prototype;
                this.observer = observer;
            }

            @Override
            public void onClick(View v) {
                if (!isDisposed()) {
                    try {
                       observer.onNext((KeyAction) prototype.clone());
                    } catch (CloneNotSupportedException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            protected void onDispose() {
                view.setOnClickListener(null);
            }
        }
    }
}
