package com.dartsukevich.currencyconverter.repository.persistence;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

import com.dartsukevich.currencyconverter.repository.persistence.dao.CurrencyDao;
import com.dartsukevich.currencyconverter.repository.persistence.dao.RatesDao;
import com.dartsukevich.currencyconverter.repository.persistence.model.Currency;
import com.dartsukevich.currencyconverter.repository.persistence.model.CurrencyRate;

/**
 * Created by User on 1/30/2018.
 */

@Database(entities = {Currency.class, CurrencyRate.class}, version = 1)
@TypeConverters(Converters.class)
public abstract class CurrencyDatabase extends RoomDatabase{
    public abstract CurrencyDao currencyDao();
    public abstract RatesDao ratesDao();
}
