package com.dartsukevich.currencyconverter.repository.persistence;

import android.app.Application;
import android.arch.persistence.room.Room;

import com.dartsukevich.currencyconverter.repository.persistence.dao.CurrencyDao;
import com.dartsukevich.currencyconverter.repository.persistence.dao.RatesDao;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by User on 1/30/2018.
 */

@Module
public class PersistenceModule {
    @Provides
    @Singleton
    CurrencyDatabase provideFeedsDatabase(Application application) {
        return Room.databaseBuilder(application, CurrencyDatabase.class, "currency_database").build();
    }

    @Provides
    @Singleton
    CurrencyDao provideCurrencyDao(CurrencyDatabase database) {
        return database.currencyDao();
    }

    @Provides
    @Singleton
    RatesDao provideRatedDao(CurrencyDatabase database) {
        return database.ratesDao();
    }
}