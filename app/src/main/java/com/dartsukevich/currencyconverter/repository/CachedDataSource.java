package com.dartsukevich.currencyconverter.repository;


import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;

/**
 * Created by User on 1/30/2018.
 */

abstract class CachedDataSource<DataType, RequestType> {

    Observable<DataType> load() {
        return loadFromDb().map(new Function<DataType, Result<DataType>>() {
            @Override
            public Result<DataType> apply(DataType dataType) throws Exception {
                return new Result<>(dataType);
            }
        }).defaultIfEmpty(new Result<DataType>(null)).flatMap(new Function<Result<DataType>, ObservableSource<DataType>>() {
            @Override
            public ObservableSource<DataType> apply(Result<DataType> result) throws Exception {
                if (shouldFetchFromNetwork(result)) {
                    return fetchFromNetwork().flatMap(new Function<RequestType, ObservableSource<DataType>>() {
                        @Override
                        public ObservableSource<DataType> apply(RequestType request) throws Exception {
                            save(request);
                            return loadFromDb();
                        }
                    });
                }
                return Observable.just(result.data);
            }
        });
    }

    abstract Observable<DataType> loadFromDb();

    abstract boolean shouldFetchFromNetwork(Result<DataType> dataType);

    abstract Observable<RequestType> fetchFromNetwork();

    abstract void save(RequestType request);

    class Result<T> {
        T data;

        public Result(T data) {
            this.data = data;
        }
    }
}
