package com.dartsukevich.currencyconverter.repository.persistence.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.dartsukevich.currencyconverter.repository.persistence.model.CurrencyRate;

import io.reactivex.Flowable;
import io.reactivex.Maybe;

/**
 * Created by User on 1/30/2018.
 */

@Dao
public interface RatesDao {

    @Query("SELECT * FROM rates where currencyId=:currencyId")
    Maybe<CurrencyRate> getRate(int currencyId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(CurrencyRate currencyRate);
}
