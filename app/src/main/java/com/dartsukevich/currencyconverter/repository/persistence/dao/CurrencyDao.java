package com.dartsukevich.currencyconverter.repository.persistence.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.dartsukevich.currencyconverter.repository.persistence.model.Currency;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by User on 1/30/2018.
 */

@Dao
public interface CurrencyDao {

    @Query("SELECT * FROM currencies")
    Flowable<List<Currency>> getAllCurrencies();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Currency> currencyList);
}
