package com.dartsukevich.currencyconverter.repository;

import com.dartsukevich.currencyconverter.network.rest.CurrencyApi;
import com.dartsukevich.currencyconverter.network.rest.model.RESTCurrency;
import com.dartsukevich.currencyconverter.network.rest.model.RESTCurrencyRate;
import com.dartsukevich.currencyconverter.repository.persistence.CurrencyDatabase;
import com.dartsukevich.currencyconverter.repository.persistence.dao.CurrencyDao;
import com.dartsukevich.currencyconverter.repository.persistence.dao.RatesDao;
import com.dartsukevich.currencyconverter.repository.persistence.model.Currency;
import com.dartsukevich.currencyconverter.repository.persistence.model.CurrencyRate;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Maybe;
import io.reactivex.MaybeObserver;
import io.reactivex.MaybeSource;
import io.reactivex.Observable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Function;

/**
 * Created by User on 1/30/2018.
 */

public class CurrencyRepository {

    private CurrencyDao mCurrencyDao;
    private RatesDao mRatesDao;
    private CurrencyDatabase mDatabase;

    private CurrencyApi mCurrencyApi;

    @Inject
    public CurrencyRepository(CurrencyDao currencyDao, RatesDao ratesDao, CurrencyDatabase database, CurrencyApi currencyApi) {
        this.mCurrencyDao = currencyDao;
        this.mRatesDao = ratesDao;
        this.mDatabase = database;
        this.mCurrencyApi = currencyApi;
    }

    public Observable<List<Currency>> loadCurrencies() {
        return new CachedDataSource<List<Currency>, List<RESTCurrency>>() {

            @Override
            Observable<List<Currency>> loadFromDb() {
                return Observable.fromPublisher(mCurrencyDao.getAllCurrencies());
            }

            @Override
            boolean shouldFetchFromNetwork(Result<List<Currency>> currencies) {
                return currencies.data == null || currencies.data.size() == 0;
            }

            @Override
            Observable<List<RESTCurrency>> fetchFromNetwork() {
                return mCurrencyApi.getCurrencyList();
            }

            @Override
            void save(List<RESTCurrency> request) {
                List<Currency> currencies = new ArrayList<>();
                for (RESTCurrency restCurrency : request) {
                    currencies.add(convert(restCurrency));
                }
                mDatabase.beginTransaction();
                try {
                    mCurrencyDao.insertAll(currencies);
                    mDatabase.setTransactionSuccessful();
                } finally {
                    mDatabase.endTransaction();
                }
            }
        }.load();
    }

    public Observable<CurrencyRate> loadExchangeRate(final int internalCode) {
        return new CachedDataSource<CurrencyRate, RESTCurrencyRate>() {
            @Override
            Observable<CurrencyRate> loadFromDb() {
                return mRatesDao.getRate(internalCode).toObservable();
            }

            @Override
            boolean shouldFetchFromNetwork(Result<CurrencyRate> currencyRate) {
                return currencyRate.data == null;
            }

            @Override
            Observable<RESTCurrencyRate> fetchFromNetwork() {
                return mCurrencyApi.getExchangeRate(internalCode);
            }

            @Override
            void save(RESTCurrencyRate request) {
                CurrencyRate convert = convert(request);
                mDatabase.beginTransaction();
                try {
                    mRatesDao.insert(convert);
                    mDatabase.setTransactionSuccessful();
                } finally {
                    mDatabase.endTransaction();
                }
            }
        }.load();
    }

    private Currency convert(RESTCurrency restCurrency) {
        Currency currency = new Currency();
        currency.setAbbreviation(restCurrency.getAbbreviation());
        currency.setCode(restCurrency.getCode());
        currency.setCurrencyId(restCurrency.getCurrencyId());
        currency.setDateEnd(restCurrency.getDateEnd());
        currency.setDateStart(restCurrency.getDateStart());
        currency.setPeriodicity(restCurrency.getPeriodicity());
        currency.setName(restCurrency.getName());
        return currency;
    }

    private CurrencyRate convert(RESTCurrencyRate restCurrencyRate) {
        CurrencyRate rate = new CurrencyRate();
        rate.setCurrencyId(restCurrencyRate.getCurrencyId());
        rate.setRate(restCurrencyRate.getRate());
        rate.setScale(restCurrencyRate.getScale());
        rate.setDate(restCurrencyRate.getDate());
        return rate;
    }
}
